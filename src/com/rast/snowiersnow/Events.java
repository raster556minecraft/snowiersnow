package com.rast.snowiersnow;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.Snow;
import org.bukkit.entity.FallingBlock;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockFadeEvent;
import org.bukkit.event.block.BlockPhysicsEvent;
import org.bukkit.event.entity.EntityDropItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitScheduler;

public class Events
        implements Listener {
    private final SnowierSnow plugin = SnowierSnow.getPlugin();
    private final BukkitScheduler scheduler = Bukkit.getScheduler();


    @EventHandler
    public void onBlockUpdate(BlockPhysicsEvent e) {
        Block block = e.getBlock();

        if (block.getType().equals(Material.SNOW)) {
            Snow snow = (Snow) block.getBlockData();
            if (snow.getLayers() == snow.getMaximumLayers()) {
                block.setType(Material.SNOW_BLOCK);
            }
        }


        if (block.getType().equals(Material.SNOW_BLOCK) || block.getType().equals(Material.SNOW)) {
            Material blockDownType = block.getRelative(BlockFace.DOWN).getType();
            if (blockDownType.equals(Material.AIR) || blockDownType.equals(Material.CAVE_AIR) || blockDownType.equals(Material.VOID_AIR) || blockDownType.equals(Material.WATER)) {
                BlockData data = block.getBlockData().clone();

                scheduler.runTaskLater(plugin, () -> {
                    if (block.getType().equals(Material.SNOW_BLOCK) || block.getType().equals(Material.SNOW)) {
                        block.setType(Material.AIR);
                        FallingBlock fallingBlock = block.getWorld().spawnFallingBlock(block.getLocation().add(0.5, 0.0, 0.5), data);
                        fallingBlock.setDropItem(true);
                    }
                }, 2L);
            }
        }
    }


    @EventHandler
    public void onFallingBlockItem(EntityDropItemEvent e) {
        if (e.getEntity() instanceof FallingBlock) {
            FallingBlock fallingBlock = (FallingBlock) e.getEntity();
            if (fallingBlock.getBlockData().getMaterial().equals(Material.SNOW_BLOCK)) {
                fallingBlock.getWorld().dropItem(fallingBlock.getLocation(), new ItemStack(Material.SNOWBALL, 2));
                e.setCancelled(true);
            } else if (fallingBlock.getBlockData().getMaterial().equals(Material.SNOW)) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onGrassDecay(BlockFadeEvent e) {
        if (e.getBlock().getType().equals(Material.GRASS_BLOCK) &&
                (e.getBlock().getRelative(BlockFace.UP).getType().equals(Material.SNOW) || e.getBlock().getRelative(BlockFace.UP).getType().equals(Material.SNOW_BLOCK))) {
            e.setCancelled(true);
        }
    }
}