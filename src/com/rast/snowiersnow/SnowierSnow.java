package com.rast.snowiersnow;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class SnowierSnow extends JavaPlugin {
    private static SnowierSnow plugin;
    private SudoGameTick sudoGameTick;
    private double snowGenChance;
    private List<String> worlds = new ArrayList<>();


    public void onEnable() {
        plugin = this;
        plugin.saveDefaultConfig();
        FileConfiguration config = plugin.getConfig();


        this.snowGenChance = config.getDouble("snowGenChance");
        this.worlds = config.getStringList("worlds");

        this.sudoGameTick = new SudoGameTick();
        getServer().getPluginManager().registerEvents(new Events(),  this);
    }


    public void onDisable() {
        this.sudoGameTick.stopGameTick();
    }


    public static SnowierSnow getPlugin() {
        return plugin;
    }


    public double getSnowGenChance() {
        return this.snowGenChance;
    }

    public List<String> getWorlds() {
        return this.worlds;
    }
}