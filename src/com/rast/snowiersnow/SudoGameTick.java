package com.rast.snowiersnow;

import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.type.Snow;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockFormEvent;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.noise.PerlinNoiseGenerator;

class SudoGameTick {
    private int viewDistance = Math.min(Bukkit.getViewDistance(), 7);
    private BukkitTask gameTickTask;
    private Random random = new Random(System.currentTimeMillis());
    private double snowGenChance = SnowierSnow.getPlugin().getSnowGenChance();
    private Set<Chunk> chunkSet = new HashSet<>();
    private List<String> worlds = SnowierSnow.getPlugin().getWorlds();
    private Block f;
    private Block b;
    private Block l;
    private Block r;
    private Block fr;
    private Block fl;
    private Block br;
    private Block bl;
    private int chunkX;
    private int chunkZ;
    private int blockX;
    private int blockZ;
    private Block randomBlock;
    private Snow snow;
    private int snowTotal = 0;

    private World world;
    private Location loc;

    public SudoGameTick() {
        gameTickTask = Bukkit.getScheduler().runTaskTimer(SnowierSnow.getPlugin(), this::gameTick, 1L, 1L);
    }


    private void gameTick() {
        chunkSet.clear();


        for (Player player : Bukkit.getOnlinePlayers()) {
            world = player.getWorld();
            loc = player.getLocation();

            if (worlds.contains(world.getName())) {
                if (world.getEnvironment().equals(World.Environment.NORMAL) && world.hasStorm()) {
                    chunkX = loc.getChunk().getX();
                    chunkZ = loc.getChunk().getZ();


                    for (int x = chunkX - viewDistance; x <= chunkX + viewDistance; x++) {
                        for (int z = chunkZ - viewDistance; z <= chunkZ + viewDistance; z++) {
                            chunkSet.add(world.getChunkAt(x, z));
                        }
                    }
                }
            }
        }


        for (Chunk workingChunk : chunkSet) {
            if (workingChunk.isLoaded() && random.nextFloat() <= snowGenChance) {
                chunkX = workingChunk.getX();
                chunkZ = workingChunk.getZ();
                blockX = randomRange(chunkX * 16, chunkX * 16 + 16);
                blockZ = randomRange(chunkZ * 16, chunkZ * 16 + 16);
                randomBlock = workingChunk.getWorld().getHighestBlockAt(blockX, blockZ).getRelative(BlockFace.UP);
                if (randomBlock.getTemperature() <= 0.15 && randomBlock.getLightFromBlocks() <= 10) {
                    if (randomBlock.getType().equals(Material.SNOW)) {
                        snow = (Snow) randomBlock.getBlockData();
                        if (getSnowAverage(randomBlock) >= snow.getLayers() - 0.2 &&
                                snow.getLayers() < snow.getMaximumLayers()) {
                            snow.setLayers(snow.getLayers() + 1);
                            randomBlock.setBlockData(snow);
                            Bukkit.getPluginManager().callEvent(new BlockFormEvent(randomBlock, randomBlock.getState()));
                        }
                    }
                }
            }
        }
    }


    private double getSnowAverage(Block block) {
        snowTotal = 0;
        f = block.getRelative(BlockFace.NORTH);
        b = block.getRelative(BlockFace.SOUTH);
        l = block.getRelative(BlockFace.EAST);
        r = block.getRelative(BlockFace.WEST);
        fr = block.getRelative(BlockFace.NORTH_EAST);
        fl = block.getRelative(BlockFace.NORTH_WEST);
        br = block.getRelative(BlockFace.SOUTH_EAST);
        bl = block.getRelative(BlockFace.SOUTH_WEST);
        if (f.getType().equals(Material.SNOW)) {
            snow = (Snow) f.getBlockData();
            snowTotal += snow.getLayers();
        } else if (f.getType().isSolid()) {
            snowTotal += 8;
        }
        if (b.getType().equals(Material.SNOW)) {
            snow = (Snow) b.getBlockData();
            snowTotal += snow.getLayers();
        } else if (b.getType().isSolid()) {
            snowTotal += 8;
        }
        if (l.getType().equals(Material.SNOW)) {
            snow = (Snow) l.getBlockData();
            snowTotal += snow.getLayers();
        } else if (l.getType().isSolid()) {
            snowTotal += 8;
        }
        if (r.getType().equals(Material.SNOW)) {
            snow = (Snow) r.getBlockData();
            snowTotal += snow.getLayers();
        } else if (r.getType().isSolid()) {
            snowTotal += 8;
        }
        if (fr.getType().equals(Material.SNOW)) {
            snow = (Snow) fr.getBlockData();
            snowTotal += snow.getLayers();
        } else if (fr.getType().isSolid()) {
            snowTotal += 8;
        }
        if (fl.getType().equals(Material.SNOW)) {
            snow = (Snow) fl.getBlockData();
            snowTotal += snow.getLayers();
        } else if (fl.getType().isSolid()) {
            snowTotal += 8;
        }
        if (br.getType().equals(Material.SNOW)) {
            snow = (Snow) br.getBlockData();
            snowTotal += snow.getLayers();
        } else if (br.getType().isSolid()) {
            snowTotal += 8;
        }
        if (bl.getType().equals(Material.SNOW)) {
            snow = (Snow) bl.getBlockData();
            snowTotal += snow.getLayers();
        } else if (bl.getType().isSolid()) {
            snowTotal += 8;
        }

        return snowTotal / 8.0D;
    }


    private int randomRange(int i1, int i2) {
        return i1 + (int) Math.floor((random.nextFloat() * (i2 - i1)));
    }


    public void stopGameTick() {
        gameTickTask.cancel();
    }
}